import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Profile from './views/profile/Profile';
import PilotDetails from './views/profile/pilot/PersonalDetails';
import Logbook from './views/profile/pilot/LogBook';
import CompanyDetails from './views/profile/company/CompanyDetails';
import Jobs from './views/profile/company/Jobs';

import AddFlight from './views/profile/pilot/logbook/AddFlight';
import MyLogs from './views/profile/pilot/logbook/MyLogs';
import {Constants} from '@/globals/constants';


Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
            children: [
                {
                    path: 'pilot-details',
                    name: 'PilotDetails',
                    component: PilotDetails,
                    meta: {type: Constants.user_type_pilot}
                },
                {
                    path: 'logbook',
                    name: 'logbook',
                    component: Logbook,
                    meta: {type: Constants.user_type_pilot},
                    children: [
                        {
                            path: 'add-flight',
                            name: 'AddFlightView',
                            component: AddFlight,
                            meta: {type: Constants.user_type_pilot}
                        },
                        {
                            path: 'my-logs',
                            name: 'MyLogsView',
                            component: MyLogs,
                            meta: {type: Constants.user_type_pilot}
                        }
                    ]
                },
                {
                    path: 'company-details',
                    name: 'CompanyDetails',
                    component: CompanyDetails,
                    meta: {type: Constants.user_type_company}
                },
                {
                    path: 'company-jobs',
                    name: 'Jobs',
                    component: Jobs,
                    meta: {type: Constants.user_type_company}
                },
            ]
        }
    ]
});

// router.beforeEach((to, from, next) => {
//     const asd = State.getAccountType();
//   const lang = to.params.lang;
//     next()
//   // loadLanguageAsync(lang).then(() => next());
// });

export default router;
