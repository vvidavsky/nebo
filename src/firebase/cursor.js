/**
 *
 * Written by vlad on 29/08/2018
 */

export default class Cursor {
    constructor(baseRef, pageSize, sortParam) {
        this.baseRef = baseRef;
        this.startAt = new Date();
        this.pageSize = pageSize;
        this.sortParam = sortParam;
    }

    /**
     * Function that gets next page data
     * @param customStartAt - timestamp
     * @returns {Promise<firebase.database.DataSnapshot>}
     */
    next(customStartAt = this.startAt.getTime()) {
        return this.baseRef
            .ref()
            .endAt(customStartAt)
            .limitToLast(this.pageSize)
            .orderByChild(this.sortParam);
    }

    /**
     * Function that gets previous page data
     * @param customStartAt - timestamp
     * @returns {Promise<firebase.database.DataSnapshot>}
     */
    previous(customStartAt) {
        return this.baseRef
            .ref()
            .startAt(customStartAt)
            .limitToFirst(this.pageSize)
            .orderByChild(this.sortParam);
    }

    /**
     * Method that makes received data manipulations to prepare it
     * for correct exposition
     * @param snapshot - collection of received page data
     * @returns {*[]}
     */
    operateData(snapshot) {
        const dataToReturn = [];
        if (snapshot.val()) {
            snapshot.forEach((child) => {
                const log = child.val();
                log.key = child.key;
                dataToReturn.push(log);
            });
        }
        return dataToReturn.reverse();
    }
}