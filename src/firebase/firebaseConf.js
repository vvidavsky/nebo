/**
 *
 * Written by vlad on 25/06/2018
 */

import Firebase from 'firebase/app';
import 'firebase/database';

const config = {
  apiKey: 'AIzaSyDIn-XTsQOB0EjfZ1eZNLegWZ9i9wcjeNo',
  authDomain: 'skyjobs-cf4e2.firebaseapp.com',
  databaseURL: 'https://skyjobs-cf4e2.firebaseio.com',
  projectId: 'skyjobs-cf4e2',
  storageBucket: 'skyjobs-cf4e2.appspot.com',
  messagingSenderId: '582502393644'
};

Firebase.initializeApp(config);

export default Firebase;