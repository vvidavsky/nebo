/**
 *
 * Written by vlad on 26/06/2018
 */

import Firebase from './firebaseConf';
import 'firebase/auth';

/**
 * @class Authentication
 */
export default class Authentication {

    /**
     * @static
     * @return {firebase.auth.Auth | *}
     */
    static auth() {
        return Firebase.auth();
    }

    /**
     * @static
     * @param {string} email
     * @param {string} password
     * @return {Promise<void>}
     */
    static async signup(email, password) {
        return await Authentication.auth().createUserWithEmailAndPassword(email, password);
    }

    /**
     * Firebase login method
     * @param {string} email
     * @param {string} password
     * @return {Promise<void>}
     */
    static async login(email, password) {
        return await Authentication.auth().signInWithEmailAndPassword(email, password);
    }

    /**
     * @static
     * @return {Promise<void>}
     */
    static async logout() {
        const status = await Authentication.auth().signOut();
        console.log(status);
    }

    /**
     * @static
     * @param email
     * @return {Promise<void>}
     */
    static async sendResetPasswordRequest(email) {
        const status = await Authentication.auth().sendPasswordResetEmail(email);
        console.log(status);
    }

    /**
     * @static
     * @return {Promise<void>}
     */
    static async sendUserEmailVerification() {
        const status = await Authentication.auth().currentUser.sendEmailVerification();
        console.log(status);
    }

    /**
     * @static
     * @return {Promise<firebase.User>}
     */
    static async currentUser() {
        return Authentication.auth().currentUser;
    }

    /**
     * @static
     * @return {Promise<void>}
     */
    static async isLoggedIn() {
        Authentication.auth().onAuthStateChanged(user => user);
    }
}