/**
 *
 * Written by vlad on 19/11/2018
 */

import Vue from 'vue'
import Vuex from 'vuex'
import Connection from "../firebase/connection";

Vue.use(Vuex);

/**
 * Here we hold all general application data accessible from any component
 * Vlad. 21/11/18
 * @type {Store}
 */
export const store = new Vuex.Store({
    state: {
        main_app_spinner: null,
        current_user_id: null,
        current_user_email: null,
        current_user_name: null,
        current_user_type: null,
        open_log_data: null,
        profile_links: null,
        async_request_sent: false,
        airplanes_data: null,
        highlight_field: false,

        /********* FIREBASE REFERENCES *********/
        logbook_firebase_ref: null,
        user_details_firebase_ref: null,
        autocomplete_airlines_firebase_ref: null,
        airplanes_data_firebase_ref: null,
        /******* & FIREBASE REFERENCES & *******/

    },
    getters: {
        getData(state) {
            return state.current_user_id;
        },
        getAccountType(state) {
            return state.current_user_type;
        }
    },
    mutations: {
        setVuexAppSpinner(state, value = null) {
            state.main_app_spinner = value;
        },
        setVuexCurrentUserID(state, uid) {
            state.current_user_id = uid;
        },
        setVuexCurrentUserEmail(state, email) {
            state.current_user_email = email;
        },
        setVuexCurrentUserName(state, name) {
            state.current_user_name = name;
        },
        setVuexCurrentUserType(state, type) {
            state.current_user_type = type;
        },
        setVuexOpenLogData(state, value) {
            state.open_log_data = value;
        },
        setVuexProfileLinks(state, links) {
            state.profile_links = links;
        },
        setVuexAsyncRequestSentFlag(state, value) {
            state.async_request_sent = value;
        },
        setVuexAirplanesData(state, data) {
          state.airplanes_data = data;
        },
        setVuexHighlightField(state) {
          state.highlight_field = true;
          setTimeout(() => {
              state.highlight_field = false;
          }, 2000);
        },
        setVuexLogbookReference(state, reference) {
            if (!state.logbook_firebase_ref) {
                state.logbook_firebase_ref = new Connection(reference);
            }
        },
        setVuexUserDetailsReference(state, reference) {
            if (!state.user_details_firebase_ref) {
                state.user_details_firebase_ref = new Connection(reference);
            }
        },
        setVuexAirlinesReference(state, reference) {
            if (!state.autocomplete_airlines_firebase_ref) {
                state.autocomplete_airlines_firebase_ref = new Connection(reference);
            }
        },
        setVuexAirplanesReference(state, reference) {
            if (!state.airplanes_data_firebase_ref) {
                state.airplanes_data_firebase_ref = new Connection(reference);
            }
        }

    }
});