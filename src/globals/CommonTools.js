/**
 * Written by vlad on 05/01/2019
 */

export default class AppTools {
    /**
     * Фунцтия вытаскивает читаемое значение данных о самолете
     * производителе и моделе согласно полученному коды
     * @param dataArray массив с данными согласно типу или производителю в котором нужно искать значение
     * @param code код производителя либо модели по которому нужно найти читаемое значение
     * @returns String
     * Vlad. 05/01/19
     */
    static getReadablePlaneValues(dataArray, code) {
        return dataArray.find((item) => {
            return item.code === code
        }).text;
    };
}