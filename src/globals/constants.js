/**
 *
 * Written by vlad on 13/11/2018
 */

export const Constants = {
    /*********** FIREBASE TABLES REFERENCES *************/
    fire_user_details: "user_details",
    fire_log_book: "/log_book",
    fire_app_strings: "i18n",
    fire_personal_data: "/personal_data",
    fire_airlines_data: "/common_data/airlines",
    fire_registered_airlines: "/common_data/registered_airlines",
    fire_airplanes: "/common_data/airplanes",
    fire_inserted_details: "/inserted_details", // object inside user details table containing details inserted by user
    /********* & FIREBASE TABLES REFERENCES & ***********/

    log_items_limit: 50, // Limit of the items queried from firebase
    user_type_pilot: "PILOT",
    user_type_company: "COMPANY",
    PILOT_profile_links: [
        {name: "message.details", route: "/profile/pilot-details"},
        {name: "message.logbook", route: "/profile/logbook/add-flight"}
    ],
    COMPANY_profile_links: [
        {name: "message.details", route: "/profile/company-details"},
        {name: "message.jobs", route: "/profile/company-jobs"}
    ]
};