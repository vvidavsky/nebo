/**
 * Written by vlad on 13/07/2018
 */

import VueI18n from "vue-i18n";
import Connection from "../firebase/connection";
import axios from "axios";

import {Constants} from "../globals/constants";
export default class Localization {

    constructor(lang) {

        /**
         * @property Localization
         * @type {VueI18n}
         */
        this.i18n = new VueI18n({
            locale: lang, // set locale
            fallbackLocale: lang,
            messages: Localization.getStrings()[lang] // set locale messages
        });
        // our default language that is preloaded
        this.loadedLanguages = [lang];
    }

    setI18nLanguage(lang) {
        this.i18n.locale = lang;
        axios.defaults.headers.common["Accept-Language"] = lang;
        document.querySelector("html").setAttribute("lang", lang);
        return lang;
    }

    async loadLanguageAsync(lang) {
        const that = this;
        if (that.i18n.locale !== lang) {
            if (!that.loadedLanguages.includes(lang)) {
                const appStrings = new Connection(Constants.fire_app_strings);
                const snap = await appStrings.ref().once("value");
                that.i18n.setLocaleMessage(lang, snap.val()[lang]);
                that.loadedLanguages.push(lang);
                return that.setI18nLanguage(lang);
            }
            return await that.setI18nLanguage(lang);
        }
        return await lang;
    }

    /**
     * Application Strings
     * @returns {{en: {message: {hello: string}}, ru: {message: {hello: string}}}}
     */
    static getStrings() {
        return {
            en: {
                message: {
                    app_loading_message: "Starting up the engines..."
                }
            },
            ru: {
                message: {
                    app_loading_message: "От Винта..."
                }
            }
        };
    }
}