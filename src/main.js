import Vue from 'vue';
import App from './App.vue';
import router from './router';
import VueFire from 'vuefire';
import VModal from 'vue-js-modal';
import Notifications from 'vue-notification';
import VueI18n from 'vue-i18n';
import Localization from '@/localization/localization';
import VeeValidate from 'vee-validate';
import VueMoment from 'vue-moment';
import { VueSpinners } from '@saeris/vue-spinners'
import {store} from "@/globals/vuexConfig";

import InputComp from '@/components/ui-components/nb-input-component';
import DatepickerComp from '@/components/ui-components/nb-datepicker-component';
import NationalPickerComp from '@/components/ui-components/nb-nat-picker-component';
import CountryPickerComp from '@/components/ui-components/nb-country-picker-component';
import PickerComp from '@/components/ui-components/nb-picker-component';
import CheckboxComp from '@/components/ui-components/nb-checkbox-component';
import DigitsComp from '@/components/ui-components/nb-digits-component';
import TextComp from '@/components/ui-components/nb-textarea-component';
import AirlinePickerComp from '@/components/ui-components/airline-picker/nb-airline-picker-component';
import Preloader from '@/components/ui-components/nb-app-preloader';
import LabelComponent from '@/components/ui-components/nb-label-component';
import VTooltip from "v-tooltip";
import Autocomplete from 'v-autocomplete'

Vue.use(VueFire);
Vue.use(Notifications);
Vue.use(VueSpinners);
Vue.use(VModal, {dynamic: true});
Vue.use(VueI18n);
Vue.use(VeeValidate);
Vue.use(VueMoment);
Vue.use(VTooltip);
Vue.use(Autocomplete);

Vue.component('nb-input', InputComp);
Vue.component('nb-datepicker', DatepickerComp);
Vue.component('nb-nat-picker', NationalPickerComp);
Vue.component('nb-country-picker', CountryPickerComp);
Vue.component('nb-picker', PickerComp);
Vue.component('nb-checkbox', CheckboxComp);
Vue.component('nb-digits', DigitsComp);
Vue.component('nb-textarea', TextComp);
Vue.component('nb-airline-picker', AirlinePickerComp);
Vue.component('nb-preloader', Preloader);
Vue.component('nb-form-label', LabelComponent);

Vue.config.productionTip = false;


const localize = new Localization('');
Promise.resolve(localize.loadLanguageAsync('en')).then(() => {
    store.commit("setVuexAppSpinner");
});

const i18n = localize.i18n;

new Vue({
    i18n,
    store,
    router,
    render: h => h(App)
}).$mount('#app');
