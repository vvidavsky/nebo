/**
 *
 * Written by vlad on 08/07/2018
 */
export default class Notifications {

    /**
     * Success notification
     * @param context - stupid way to pass context
     * @param title - String
     * @param text - String
     */
    static showSuccess(context, title, text) {
        context.$notify({
            group: 'app-default',
            title: title,
            text: text,
            type: 'success',
            duration: 5000
        });
    }

    /**
     * Warning notification
     * @param context - stupid way to pass context
     * @param title - String
     * @param text - String
     */
    static showWarning(context, title, text) {
        context.$notify({
            group: 'app-default',
            title: title,
            text: text,
            type: 'warn',
            duration: 5000
        });
    }

    /**
     * Error notification
     * @param context - stupid way to pass context
     * @param title - String
     * @param text - String
     */
    static showError(context, title, text) {
        context.$notify({
            group: 'app-default',
            title: title,
            text: text,
            type: 'error',
            duration: 5000
        });
    }
}